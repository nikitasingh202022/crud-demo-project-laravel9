<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $faker = Faker::create();
        foreach (range(1, 200) as $index) {
            DB::table('student_models')->insert([
                'studentName' => $faker->studentName,
                'studentFather' => $faker->studentFather,
                'studentMother' => $faker->studentMother,
                'studentfee' => $faker->studentfee,
                'studentAddress' => $faker->studentAddress,
            ]);
        }
    }
}
