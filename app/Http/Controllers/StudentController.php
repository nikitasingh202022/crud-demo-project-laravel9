<?php

namespace App\Http\Controllers;

use App\Models\StudentModel;
use App\Repositry\ICommonRepositry;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DataTables;

class StudentController extends Controller
{
    public $student;

    public function __construct(ICommonRepositry $student)
    {
        $this->student = $student;
    }

    public function addStudent(Request $request)
    {
        // $data = array();
        // $id = $this->uri->segment(2);
        // if (isset($id) && $id != "") {
        //     $data['subMenu'] = $this->student->getDataById('student_models', '*', array('id' => $id));
        //     if (empty($data['subMenu'])) {
        //         exit();
        //     }
        // }
        // print_r($id);
        //  die;
        // $data = $this->student->getDataById("student_models", "*", $id);
        // print_r($data);
        // die;

        return view('Dashbord.addStudent');
    }

    public function action()
    {
        return view('Dashbord.action');
    }
    public function insertStudentData(Request $request)
    {
        $validation = $request->validate([
            'studentName' => 'required',
            'studentFather' => 'required',
            'studentMother' => 'required',
            'studentfee' => 'required',
            'studentAddress' => 'required'
        ]);

        if ($validation) {
            $data['studentName'] = $request->input('studentName');
            $data['studentFather'] = $request->input('studentFather');
            $data['studentMother'] = $request->input('studentMother');
            $data['studentfee'] = $request->input('studentfee');
            $data['studentAddress'] = $request->input('studentAddress');
            if (isset($request['id']) && $request['id'] != '') {
                $id = $request['id'];
                $updateStudentData = $this->student->updateData('student_models', $id, $data);
                if ($updateStudentData) {
                    return redirect("/studentList");
                } else {
                    return "Not Update";
                }
            } else {
                $result = $this->student->insertDataWithUuid('student_models', $data);
                if ($result) {
                    return redirect("/addStudent");
                } else {
                    return "Not Insert";
                }
            }
        } else {
            return redirect('/addStudent');
        }
    }

    public function deleteData(Request $request)
    {
        $query = $this->student->deleteData('student_models', $request->id);
        if ($query) {
            return $query;
        } else {
            return "Faild";
        }
    }
    public function getStudents(Request $request)
    {

        if (request()->ajax()) {
            $data = $this->student->getAllData('student_models');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="{{ url("/addStudent",$id) }}" data-toggle="tooltip" data-bs-toggle="modal" data-bs-target="#ajaxModel" data-original-title="Edit" class="edit btn btn-success edit">Edit</a>
                <a href="javascript:void(0)" data-id="{{ $id }}" data-toggle="tooltip" data-original-title="Delete" class="delete btn btn-danger">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('/');
    }
}
