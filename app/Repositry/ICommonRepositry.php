<?php

namespace App\Repositry;

interface ICommonRepositry
{
    public function insertData($table, $data);
    public function deleteData($table, $cond);
    public function getDataById($table,$select,$con);
    public function getAllData($table, $key = "");
    public function updateData($table, $cond, $data);
    public function executeSql($table, $sql, $type);
    public function insertDataWithUuid($table, $data);
    public function check_email_id($table, $select, $con);
    public function getAllDataWhere($table, $select, $column, $opt, $id);
}
