<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ContactController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/* -----------------Dashbord Student Fee --------------------*/
Route::get('addStudent', [StudentController::class, 'addStudent'])->name('Dashbord.addStudent');
Route::get('addStudent/{id}', [StudentController::class, 'addStudent']);
Route::get('action', [StudentController::class, 'action'])->name('Dashbord.action');
Route::post('insertStudentData', [StudentController::class, 'insertStudentData']);
Route::post('deleteData', [StudentController::class, 'deleteData']);
Route::get('getStudents', [StudentController::class, 'getStudents']);

/* Details My Contact Person */
Route::get('contactList', [ContactController::class, 'contactList'])->name('Dashbord.contactList');
Route::get('contactList/{id}', [ContactController::class, 'contactList']);
Route::get('actionContact', [ContactController::class, 'actionContact'])->name('Dashbord.actionContact');
Route::post('insertContactDetails', [ContactController::class, 'insertContactDetails']);
Route::get('getContactDetails', [ContactController::class, 'getContactDetails']);



Auth::routes();
Route::prefix('admin')->middleware(['auth'])->group(function () {
    return 1;
});
Route::get("dashbord", [App\Http\Controllers\Admin\DashbordController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
