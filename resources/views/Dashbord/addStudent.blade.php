@extends('layouts.app')
@section('content')

<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="card-header">{{ __('Add Student') }}</div>
              
                <button type="button" class="btn btn-default"  style="margin-left:auto!important;" data-bs-dismiss="modal">X</button>
             

                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form method="post" id="dtudentForm" action="{{url('/insertStudentData')}}">
                    @csrf
                    <a href="{{url('/addStudent')}}">Student List</a>
                    <div class="row mb-3">
                        <label for="studentName" class="col-md-4 col-form-label text-md-end">{{ __('Student Name') }}</label>

                        <div class="col-md-6">
                            <input id="studentName" type="text" class="form-control @error('studentName') is-invalid @enderror" name="studentName" value="{{ old('newProductName') }}" required autocomplete="newProductName" autofocus>

                            @error('studentName')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="studentFather" class="col-md-4 col-form-label text-md-end">{{ __('Student Father') }}</label>

                        <div class="col-md-6">
                            <input id="studentFather" type="text" class="form-control @error('studentFather') is-invalid @enderror" name="studentFather" value="" required autocomplete="new-studentFather">

                            @error('studentFather')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="studentMother" class="col-md-4 col-form-label text-md-end">{{ __('Student Mother') }}</label>

                        <div class="col-md-6">
                            <input id="studentMother" type="text" class="form-control @error('studentMother') is-invalid @enderror" name="studentMother" required autocomplete="new-studentMother">

                            @error('studentMother')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="studentfee" class="col-md-4 col-form-label text-md-end">{{ __('Student Fee ') }}</label>

                        <div class="col-md-6">
                            <input id="studentfee" type="number" class="form-control @error('studentfee') is-invalid @enderror" name="studentfee" value="" required autocomplete="studentfee">

                            @error('studentfee')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="studentAddress" class="col-md-4 col-form-label text-md-end">{{ __('Student Address') }}</label>

                        <div class="col-md-6">
                            <input id="studentAddress" type="text" class="form-control @error('studentAddress') is-invalid @enderror" name="studentAddress" required autocomplete="new-studentAddress">
                            <input id="id" type="hidden" name="id" value="">

                            @error('studentAddress')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" id="btn-save" onclick="dtudentForm()" class="btn btn-primary">
                                {{ __('Submit') }}
                            </button>
                            <button type="button" class=" fa-light fa-star" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<a onclick="openModel()" id="btn" class="btn btn-sm btn-warning btn-lg m-t-n-xs"><i class="fa fa-plus" title="Add Student">Add Student</i></a>
<div class="container mt-5">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <table class="table table-bordered yajra-datatable" id="">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Father Name</th>
                <th>Mother Name</th>
                <th>Fee</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    function openModel() {
        $('#btn').click(function() {
            $('#ajaxModel').modal('show');
        });
    };
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('getStudents') }}",
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'studentName',
                    name: 'studentName'
                },
                {
                    data: 'studentFather',
                    name: 'studentFather'
                },
                {
                    data: 'studentMother',
                    name: 'studentMother'
                },
                {
                    data: 'studentfee',
                    name: 'studentfee'
                },
                {
                    data: 'studentAddress',
                    name: 'studentAddress'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        });

    });
    $('body').on('click', '.delete', function() {
        if (confirm("Delete Record?") == true) {
            var id = $(this).data('id');
            // ajax
            $.ajax({
                type: "POST",
                url: "{{ url('deleteData') }}",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(res) {
                    var oTable = $('.yajra-datatable').dataTable();
                    oTable.fnDraw(false);
                }
            });
        }
    });
</script>

@endsection