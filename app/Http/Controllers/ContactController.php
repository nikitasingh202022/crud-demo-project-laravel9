<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositry\ICommonRepositry;
use DataTables;

class ContactController extends Controller
{
    public $contact;
    public function __construct(ICommonRepositry $contact)
    {
        $this->contact = $contact;
    }
    public function contactList()
    {
        return view('Dashbord.contactList');
    }

    public function insertContactDetails(Request $request)
    {
        $validation = $request->validate([
            'contactProfile' =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'contactPerson' => 'required',
            'contactNumber' => 'required|min:10',
            'contactFather' => 'required',
            'contactMother' => 'required',
            'contactAddress' => 'required',
            // 'study' => 'required',
            'numberOfH' => 'required',
            'numberOfI' => 'required'
        ]);
        if ($validation) {
            $data['contactProfile'] = $request->input('contactProfile');
            $data['contactPerson'] = $request->input('contactPerson');
            $data['contactNumber'] = $request->input('contactNumber');
            $data['contactFather'] = $request->input('contactFather');
            $data['contactMother'] = $request->input('contactMother');
            $data['contactAddress'] = $request->input('contactAddress');
            $data['study'] = $request->input('study');
            $data['numberOfH'] = $request->input('numberOfH');
            $data['numberOfI'] = $request->input('numberOfI');
            // print_r($data['contactProfile']);die;

            if ($contactProfile = $request->file('contactProfile')) {
                $destinationPath = 'images/';
                $profileImage = date('YmdHis') . "." . $contactProfile->getClientOriginalExtension();
                $contactProfile->move($destinationPath, $profileImage);
                $data['contactProfile'] = "$profileImage";


                // if($studentImage){
                if (isset($request['contactId']) && $request['contactId'] != '') {
                    $id = $request['contactId'];
                    $updateContact = $this->contact->updateData('tbl_contact', $id, $data);
                    if ($updateContact) {
                        return redirect("/contactList");
                    } else {
                        return "Update Faild";
                    }
                } else {
                    $result = $this->contact->insertDataWithUuid('tbl_contact', $data);
                    // print_r($result);die;
                    if ($result) {
                        return redirect('/contactList');
                    } else {
                        return "Insert Faild";
                    }
                }
            } else {
                return "Image Not Insert";
            }
        } else {
            return "Faild Validation";
        }
    }
    public function getContactDetails(Request $request)
    {

        if (request()->ajax()) {
            $data = $this->contact->getAllData('tbl_contact');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="{{ url("/contactList",$row["contactId"]) }}" class="edit btn btn-success edit" data-bs-toggle="modal" data-bs-target="#ajaxModel">Edit</a>
                    <a href="javascript:void(0)" data-id="{{ $contactId }}" data-original-title="Delete" class="delete btn btn-danger">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('/');
    }
}
