<?php

namespace App\Providers;

use App\Repositry\StudentRepositry;
use App\Repositry\IStudentRepositry;
use App\Repositry\ICommonRepositry;
use App\Repositry\CommonRepositry;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(IStudentRepositry::class, StudentRepositry::class);
        $this->app->bind(ICommonRepositry::class, CommonRepositry::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
