<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositry\ICommonRepositry;
use Laravel\Ui\Presets\React;

use function PHPUnit\Framework\returnValue;

class ApiController extends Controller
{
    public $student;

    public function __construct(ICommonRepositry $student)
    {
        $this->student = $student;
    }

    /*   Insert Data */
    public function addData(Request $request)
    {
        $validation = $request->validate([
            'studentName' => 'required',
            'studentFather' => 'required',
            'studentMother' => 'required',
            'studentfee' => 'required',
            'studentAddress' => 'required'
        ]);

        if ($validation) {
            $data['uuid'] = $request->input('uuid');
            $data['studentName'] = $request->input('studentName');
            $data['studentFather'] = $request->input('studentFather');
            $data['studentMother'] = $request->input('studentMother');
            $data['studentfee'] = $request->input('studentfee');
            $data['studentAddress'] = $request->input('studentAddress');
            if ($data['uuid'] == '') {
                $data['uuid'] = '';
                $result = $this->student->insertData('student_models', $data);
                return $result;
            } else {
                // $result = $this->student->insertDataWithUuid($request->table, $data);
                return "Insert Data With uuid ";
            }
        } else {
            return "Faild";
        }
    }

    /* Find All Data in Datatable */
    public function getAllData()
    {
        $data = $this->student->getAllData('student_models', array());
        if ($data) {
            return $data;
        } else {
            return "Faild";
        }
    }

    /* Delete Data */
    public function deleteData(Request $request)
    {
        $result = $this->student->deleteData($request->table, $request->studentId);
        if ($result) {
            return $result;
        } else {
            return "Faild";
        }
    }

    /* Get Data with Condition */
    public function getAllDataWhere(Request $request)
    {
        $result = $this->student->getAllDataWhere($request->table, '*', $request->column, $request->opt, $request->studentId);
        if ($result) {
            return $result;
        } else {
            return "Faild";
        }
    }
    /* Get Data By Id */
    public function getDataById(Request $request)
    {
        $query = $this->student->getDataById($request->table,"*", $request->id);
        if ($query) {
            return $query;
        } else {
            return "Faild";
        }
    }

    /* Update Data */
    public function updateData1(Request $request)
    {
        $data = [
            'studentName' => $request->studentName,
            'studentFather' => $request->studentFather,
        ];
        $condition = [
            'id' => $request->id,
        ];

        return $this->student->updateData($request->table, $condition, $data);
    }

    public function insertDataWithUuid(Request $request)
    {
        $validation = $request->validate([
            'studentName' => 'required',
            'studentFather' => 'required',
            'studentMother' => 'required',
            'studentfee' => 'required',
            'studentAddress' => 'required'
        ]);

        if ($validation) {
            $data['studentName'] = $request->input('studentName');
            $data['studentFather'] = $request->input('studentFather');
            $data['studentMother'] = $request->input('studentMother');
            $data['studentfee'] = $request->input('studentfee');
            $data['studentAddress'] = $request->input('studentAddress');

            $result = $this->student->insertDataWithUuid('student_models', $data);
            return $result;
        } else {
            return "Faild";
        }
    }

    public function executeSql(Request $request)
    {
        //         $sql = "SELECT * FROM `student_models` WHERE 1 Order by desc";
        //         $type = "result_array";
        //         $query = $this->student->executeSql($request->table, $sql,'result_array');
        //         if ($query) {
        //             return $query;
        //         } else {
        //             return "Faild";
        //         }
        // }
    }
    public function check_email_id(Request $request)
    {
        $data = $this->student->check_email_id($request->table, '*', $request->id);
        if ($data) {
            return ($data->num_rows() > 0) ? $data->row_array() : 0;
        } else {
            return "Faild";
        }
    }
    
}
