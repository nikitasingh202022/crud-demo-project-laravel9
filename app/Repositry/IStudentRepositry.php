<?php

namespace App\Repositry;

interface IStudentRepositry
{
    public function getAllData();
    public function getAllWherData($id);
    public function insertData(array $data);
    public function deleteData($id);
}
