@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <center><h4>Edit</h4></center>
                    <form method="post" id="dtudentForm" action="{{url('/insertStudentData')}}">
                        @csrf
                        <a href="{{url('/studentList')}}">Student List</a>
                        <div class="row mb-3">
                            <label for="studentName" class="col-md-4 col-form-label text-md-end">{{ __('Student Name') }}</label>

                            <div class="col-md-6">
                                <input id="studentName" type="text" class="form-control @error('studentName') is-invalid @enderror" name="studentName" value="{{ old('newProductName') }}" required autocomplete="newProductName" autofocus>

                                @error('studentName')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="studentFather" class="col-md-4 col-form-label text-md-end">{{ __('Student Father') }}</label>

                            <div class="col-md-6">
                                <input id="studentFather" type="text" class="form-control @error('studentFather') is-invalid @enderror" name="studentFather" value="" required autocomplete="new-studentFather">

                                @error('studentFather')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="studentMother" class="col-md-4 col-form-label text-md-end">{{ __('Student Mother') }}</label>

                            <div class="col-md-6">
                                <input id="studentMother" type="text" class="form-control @error('studentMother') is-invalid @enderror" name="studentMother" required autocomplete="new-studentMother">

                                @error('studentMother')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="studentfee" class="col-md-4 col-form-label text-md-end">{{ __('Student Fee ') }}</label>

                            <div class="col-md-6">
                                <input id="studentfee" type="text" class="form-control @error('studentfee') is-invalid @enderror" name="studentfee" value="" required autocomplete="studentfee">

                                @error('studentfee')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="studentAddress" class="col-md-4 col-form-label text-md-end">{{ __('Student Address') }}</label>

                            <div class="col-md-6">
                                <input id="studentAddress" type="text" class="form-control @error('studentAddress') is-invalid @enderror" name="studentAddress" required autocomplete="new-studentAddress">

                                @error('studentAddress')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="btn-save" onclick="dtudentForm()" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection