<?php

namespace App\Repositry;

use App\Models\StudentModel;

class StudentRepositry implements IStudentRepositry
{

    public function getAllData()
    {
        return StudentModel::all();
    }
    public function getAllWherData($id)
    {
        return StudentModel::find($id)->select();
    }
    public function insertData(array $data)
    {
        StudentModel::insert($data);
    }
    public function deleteData($id)
    {
        return StudentModel::find($id)->delete();
    }
}
